﻿namespace DAMH.Models
{
    public class Method
    {
        public int MethodId { get; set; }
        public string? MethodName { get; set; }
    }
}
