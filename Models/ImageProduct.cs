﻿namespace DAMH.Models
{
    public class ImageProduct
    {
        public int ImageProductId { get; set; }
        public string? Url { get; set; }
        public int ProductId { get; set; }
        public Product? Product { get; set; }
    }
}
