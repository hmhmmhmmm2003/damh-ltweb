﻿using System.ComponentModel.DataAnnotations;

namespace DAMH.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string? Name { get; set; }
        public decimal Price { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public List<ImageProduct>? Images { get; set; }
        public int CategoryId { get; set; }
        public Category? Category { get; set; }
        public int SaleId { get; set; }
        public Sale? Sale { get; set; }
    }
}
