﻿namespace DAMH.Models
{
    public class Cart
    {
        public int CartId { get; set; }
        public int UserId { get; set; }
        public User? User { get; set; }
        public int ProductId { get; set; }
        public Product? product { get; set; }
        public int Quantyti { get; set; }

    }
}
