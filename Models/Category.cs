﻿using System.ComponentModel.DataAnnotations;

namespace DAMH.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        [Required, StringLength(50)]
        public string? Name { get; set; }
        public List<Product> Products { get; set; }
    }
}
