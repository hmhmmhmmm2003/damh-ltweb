﻿namespace DAMH.Models
{
    public class Payment
    {
        public int PaymentId { get; set; }
        public int OrderId { get; set; }
        public Order? Order { get; set; }
        public int? MethodsId { get; set; }
        public Method? Methods { get; set; }
        public decimal TotalPriceIm { get; set; }
        public DateTime PaymentDate { get; set; }
        public string? PaymentStatus { get; set; }
        public int SaleId { get; set; }
        public Sale? Sale { get; set; }
    }
}
