﻿namespace DAMH.Models
{
    public class Sale
    {
        public int SaleId { get; set; }
        public string? SaleName { get; set;}
        public string? SaleDescription { get; set;}
    }
}
