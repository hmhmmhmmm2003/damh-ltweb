﻿namespace DAMH.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public User? User { get; set; }

        public decimal? Price { get; set; }
        public DateTime? TimeOrder { get; set; }
        public string? Status {  get; set; }


        
    }
}
