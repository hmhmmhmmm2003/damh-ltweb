﻿using System.ComponentModel.DataAnnotations;

namespace DAMH.Models
{
    public class BestSaller
    {
        public int BestSallerId { get; set; }
        public string? Name { get; set; }
        public decimal Price { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public List<ImageProduct>? Images { get; set; }
    }
}
